Учасники проекту:
1. Патажевський Артем (Завдання для студента 1)
2. Майборода Ярослав (Завдання для студента 2)

Завдання для студента 1:

- Шапка з логотипом та меню
- Перші два блоки після секції: ім'я та зображення, опис та соціальні мережі, логотипи.
- Секція Selected work
- Секція OUr Team


Завдання для студента 2:

- Секція What I do
- Секція My clients
- Секція Contact
- Footer


Проект створювався за допомогою gulp та npm пакетів. Без сторонніх бібліотек.

Перелік пакетів:

- browser-sync
- gulp
- gulp-autoprefixer
- gulp-imagemin
- gulp-csso
- gulp-clean
- gulp-file-include
- gulp-sass
- sass


Основні завдання gulp:

- build - очищення dist, компіляція scss в css, конкатенація та мініфікованість css та js, оптимізація картинок
- dev - запуск сервера та слідкування за змінами


Посилання на сайт:

https://stepproject-artem-pat-d30919709c7d8d414b1a0dd4776df52f9cef2b8d7.gitlab.io/

